/***************************************************************************//**
 * @file
 * @brief GPIO Activation plugin for Silicon Labs bootloader.
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc.  Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement.  This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include "config/btl_config.h"

#include "em_device.h"
#include "em_gpio.h"
#include "btl_gpio_activation.h"

// Map GPIO activation polarity settings to GPIO pin states
#define HIGH 0
#define LOW  1

// doesn't allow for initialization here for some reason
volatile unsigned int hits[4];

bool gpio_enterBootloader(void)
{
  // BGG debug
  // bool pressed;
  #define NUM_BG21_DEVICES	4

  // Turns out the MAXIMUM number of hits is just below 4M hits (3,945,521)
  // The number of hits when the system is running normal without price updates is at 778,000
  // the ACTUAL numbers are getting 485,969 or fewer hits in normal mode with 1 SPI access per slot
  // the number of hits when running 'upgrade_all_bg21s.py' script on the AP5 is 3,759,504 to 3,945,734
  // 3000000 should always be a safe value
  #define	NUMBER_OF_HITS	3000000

  volatile int activated = 0;
  volatile unsigned int i;
  volatile unsigned int j;
  volatile int which_bg21;
  volatile unsigned int num_activated = 0;

#if defined(CMU_HFBUSCLKEN0_GPIO)
  // Enable GPIO clock
  CMU->HFBUSCLKEN0 |= CMU_HFBUSCLKEN0_GPIO;
#endif
#if defined(_CMU_CLKEN0_MASK)
  // Enable GPIO clock
  CMU->CLKEN0_SET = CMU_CLKEN0_GPIO;
#endif

  // Since the button may have decoupling caps, they may not be charged
  // after a power-on and could give a false positive result. To avoid
  // this issue, drive the output as an output for a short time to charge
  // them up as quickly as possible.
  GPIO_PinModeSet(BSP_BTL_BUTTON_PORT,
                  BSP_BTL_BUTTON_PIN,
                  gpioModePushPull,
                  BTL_GPIO_ACTIVATION_POLARITY);

  hits[0] = 0;
  hits[1] = 0;
  hits[2] = 0;
  hits[3] = 0;

  for (i = 0; i < 100; i++) {
    // Do nothing
  }

  // Reconfigure as an input with pull(up|down) to read the button state
  GPIO_PinModeSet(BSP_BTL_BUTTON_PORT,
                  BSP_BTL_BUTTON_PIN,
                  gpioModeInputPull,
                  BTL_GPIO_ACTIVATION_POLARITY);

  // We have to delay again here so that if the button is depressed the
  // cap has time to discharge again after being charged up by the above delay

  for (i = 0; i < 2; i++) {
    // Do nothing
  }

  // We need to do this loop once for EACH BG21.
  // The logic is, if there is a LONG string of Chip selects during any period, that's a signal
  // to THIS device that it is about to do an upgrade.

  for ( which_bg21 = 0; which_bg21 < NUM_BG21_DEVICES; which_bg21 ++ ) {
	  // Since the Activation pin will be activated many 1024 times in a row, make sure we see a long string of them
	  num_activated = 0;

	  for (i = 0; i < 5000000; i++) {  // 8000000  and 700 are too high, 600 still a bit high
		  for (j = 0; j < 2; j++) {
			  // Do nothing
		  }

		  if ( GPIO_PinInGet(BSP_BTL_BUTTON_PORT, BSP_BTL_BUTTON_PIN) == BTL_GPIO_ACTIVATION_POLARITY ) {
			  	 num_activated += 1 ;
		  }

		  // Check to see if we got activated
		  if (num_activated > NUMBER_OF_HITS) {
			  hits[which_bg21] = num_activated;
			  activated = 1;
			  // start the upgrade now!
			  break;
		  }
	  }
	  hits[which_bg21] = num_activated;
	  // for some reason, above break doesn't exit the loop!
	  if (num_activated > NUMBER_OF_HITS) break;
  }

  // Disable GPIO pin, so that it won't get activated again
  GPIO_PinModeSet(BSP_BTL_BUTTON_PORT,
                  BSP_BTL_BUTTON_PIN,
                  gpioModeDisabled,
                  BTL_GPIO_ACTIVATION_POLARITY);

#if defined(CMU_HFBUSCLKEN0_GPIO)
  // Disable GPIO clock
  CMU->HFBUSCLKEN0 &= ~CMU_HFBUSCLKEN0_GPIO;
#endif

   return activated;
}
